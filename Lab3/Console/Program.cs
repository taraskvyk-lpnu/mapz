﻿using Lab3.Models;
using Lab3.Tests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleMy
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<McDonaldsBranch> departments = DataProvider.GenerateDepartments();

            List<Employee> employees = DataProvider.GenerateEmployees(departments);

            Console.WriteLine("Список департаментiв i спiвробiтникiв:");

            foreach (var department in departments)
            {
                Console.WriteLine($"Департамент: {department.Name}");
                var employeesInDepartment = employees.Where(e => e.McDonaldsBranchId == department.McDonaldsBranchId).ToList();
                
                if (employeesInDepartment.Any())
                {
                    foreach (var employee in employeesInDepartment)
                    {
                        string format = "ID: {0}, iм'я: {1}, Вiк: {2}, Менеджер: {3}, Департамент: {4}";
                        Console.WriteLine(String.Format(format, employee.EmployeeId, employee.Name, employee.Age, employee.IsManager, department.Name));
                    }
                }
                else
                {
                    Console.WriteLine("У цьому департаментi немає спiвробiтникiв.");
                }
                Console.WriteLine();
            }
        }
    }
}