﻿using Lab3.Models;
using Lab3.Tests.Helpers;

namespace Lab3.Tests.Tests;

public class WhereTesting
{
    private List<Employee> _employees;
    private List<McDonaldsBranch> _department;

    [SetUp]
    public void Setup()
    {
        _department = DataProvider.GenerateDepartments();
        _employees = DataProvider.GenerateEmployees(_department);
    }

    [Test]
    public void Where_FilterByName_ReturnsOneEmployee()
    {
        var filteredEmployees = _employees.Where(employee => employee.Name == "John");
        
        Assert.That(filteredEmployees.Count(), Is.EqualTo(1));
        Assert.That(filteredEmployees.First().Name, Is.EqualTo("John"));
    }

    [Test]
    public void Where_FilterByAge_ReturnsOneEmployee()
    {
        var filteredEmployees = _employees.Where(employee => employee.Age > 25 && employee.Age < 35);

        Assert.That(filteredEmployees.Count(), Is.EqualTo(1));
        Assert.That(filteredEmployees.All(employee => employee.Age > 25 && employee.Age < 35), Is.True);
    }

    [Test]
    public void Where_FilterByIsManager_ReturnsTwoManagers()
    {
        var filteredEmployees = _employees.Where(employee => employee.IsManager);

        Assert.That(filteredEmployees.Count(), Is.EqualTo(2));
        Assert.That(filteredEmployees.All(employee => employee.IsManager), Is.True);
    }
    
    [Test]
    public void Where_GetManagersEmployeesFromDepartment_ReturnsListOfOneEmployee()
    {
        var employees = _employees
            .Where(employee => 
                employee.McDonaldsBranch.McDonaldsBranchId == 3 && 
                employee.IsManager);
        
        Assert.That(employees.Count(), Is.EqualTo(1));
        Assert.That(employees.FirstOrDefault().Name, Is.EqualTo("Bob"));
    }
}