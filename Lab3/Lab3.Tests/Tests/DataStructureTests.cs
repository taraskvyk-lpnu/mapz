﻿using Lab3.Models;
using Lab3.Tests.Helpers;

namespace Lab3.Tests.Tests;

public class DataStructureTests
{
    private List<McDonaldsBranch> departments;
        private List<Employee> employees;

        [SetUp]
        public void Setup()
        {
            departments = DataProvider.GenerateDepartments();
            employees = DataProvider.GenerateEmployees(departments);
        }

        [Test]
        public void SortedList_Operations()
        {
            SortedList<int, string> sortedList = new SortedList<int, string>();
            var sortedDepartments = departments.OrderByDescending(d => d.McDonaldsBranchId).ToList();

            foreach (var department in sortedDepartments)
            {
                sortedList.Add(department.McDonaldsBranchId, department.Name);
            }
            
            Assert.That(departments.Count, Is.EqualTo(sortedList.Count));

            foreach (var department in departments)
            {
                Assert.That(sortedList.ContainsKey(department.McDonaldsBranchId), Is.True);
                Assert.That(department.Name, Is.EqualTo(sortedList[department.McDonaldsBranchId]));
            }
        }

        [Test]
        public void Queue_Operations()
        {
            Queue<Employee> employeeQueue = new Queue<Employee>();

            foreach (var employee in employees)
            {
                employeeQueue.Enqueue(employee);
            }

            Assert.That(employees.Count, Is.EqualTo(employeeQueue.Count));

            foreach (var employee in employees)
            {
                var dequeuedEmployee = employeeQueue.Dequeue();
                Assert.That(employee.EmployeeId, Is.EqualTo(dequeuedEmployee.EmployeeId));
                Assert.That(employee.Name, Is.EqualTo(dequeuedEmployee.Name));
            }
        }
}
