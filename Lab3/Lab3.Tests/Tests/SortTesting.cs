﻿using Lab3.Tests.Helpers;
using Lab3.Tests.Comparers;
using Lab3.Models;

namespace Lab3.Tests.Tests;
public class SortTesting
{
    private List<Employee> _employees;
    private List<McDonaldsBranch> _departments;

    [SetUp]
    public void Setup()
    {
        _departments = DataProvider.GenerateDepartments();
        _employees = DataProvider.GenerateEmployees(_departments);
    }    
    
    [Test]
    public void SortByEmployeesByName_ReturnsSortedEmployees()
    {
        var expectedSortedEmployees = new List<string> { "Alice", "Bob", "John" };

        var actualResult = _employees.OrderBy(d => d.Name);

        Assert.That(actualResult.Select(e => e.Name).ToList(), Is.EqualTo(expectedSortedEmployees));
    }
    
    [Test]
    public void SortByEmployeesAverageAge_ReturnsSortedDepartments()
    {
        var expectedSortedDepartmentsNames = new List<string> { "HR", "Finance", "IT" };

        var actualResult = _departments.OrderBy(d => d.Employees.AverageAge());

        Assert.That(actualResult.Select(e => e.Name).ToList(), Is.EqualTo(expectedSortedDepartmentsNames));
    }
    
    [Test]
    public void SortByEmployeesCount_ReturnsSortedDepartments()
    {
        var expectedSortedDepartmentsNames = new List<string> { "IT", "HR", "Finance" };

        var actualResult = _departments.OrderByDescending(d => d.Employees.Count);

        Assert.That(actualResult.Select(e => e.Name).ToList(), Is.EqualTo(expectedSortedDepartmentsNames));
    }
}