﻿using Lab3.Models;
using Lab3.Tests.Helpers;

namespace Lab3.Tests.Tests;

public class AnonymousClasses
{
    private List<Employee> _employees;
    private List<McDonaldsBranch> _department;

    [SetUp]
    public void Setup()
    {
        _department = DataProvider.GenerateDepartments();
        _employees = DataProvider.GenerateEmployees(_department);
    }
    
    [Test]
    public void MarkAsManager_CreateAllManagersByAnonymousClass_ReturnsListWithManagers()
    {
        _employees.ForEach(e => new { Employee = e }.Employee.MarkAsManager());

        Assert.That(_employees.All(e => e.IsManager));
    }
    
    [Test]
    public void AnonymousClass_FilterByAge_ReturnsCorrectEmployees()
    { 
        var filteredByAge = _employees.Where(e => new { Age = e.Age }.Age > 25);

        Assert.That(filteredByAge.Count(), Is.EqualTo(1));
        Assert.That(filteredByAge.All(e => e.Age > 25), Is.True);
    }
    
    [Test]
    [TestCase(1, 3)]
    [TestCase(2, 3)]
    [TestCase(3, 3)]
    public void AnonymousClass_GroupByDepartmentId_ReturnsCorrectGroups(int departmentId, int expectedCount)
    {
        var groupedByDepartmentId = _employees.GroupBy(e => new { e.McDonaldsBranchId });

        Assert.That(groupedByDepartmentId.Count, Is.EqualTo(expectedCount));
        Assert.That(groupedByDepartmentId.Any(g => g.Key.McDonaldsBranchId == departmentId && g.Count() == 1), Is.True);
    }
    
    [Test]
    public void GroupByAgeAndManager_ReturnsCorrectGroups()
    {
        var groupedByAgeAndManager = _employees.GroupBy(e => new { e.Name, e.IsManager });

        var expectedCount = _employees.Count;
        Assert.That(groupedByAgeAndManager.Count, Is.EqualTo(expectedCount));
        Assert.That(groupedByAgeAndManager.Any(g => g.Key.Name == "John" && g.Key.IsManager && g.Count() == 1), Is.True);
        Assert.That(groupedByAgeAndManager.Any(g => g.Key.Name == "Alice" && !g.Key.IsManager && g.Count() == 1), Is.True);
        Assert.That(groupedByAgeAndManager.Any(g => g.Key.Name == "Bob" && g.Key.IsManager && g.Count() == 1), Is.True);
    }
}