﻿using Lab3.Models;
using Lab3.Tests.Helpers;
namespace Lab3.Tests.Tests;

public class EnumerableExtensionsTests
{
    private List<Employee> _employees;
    private List<McDonaldsBranch> _department;

    [SetUp]
    public void Setup()
    {
        _department = DataProvider.GenerateDepartments();
        _employees = DataProvider.GenerateEmployees(_department);
    }
    
    [Test]
    public void AverageAge_WhenEmptyCollection_ReturnsZero()
    {
        _employees.Clear();

        var averageAge = _employees.AverageAge();

        Assert.That(averageAge, Is.EqualTo(0));
    }
    
    [Test]
    public void AverageAge_WhenNonEmptyCollection_ReturnsCorrectAverage()
    {
        var averageAge = _employees.AverageAge();
        var expected = _employees.Average(e => e.Age);
        
        Assert.That(averageAge, Is.EqualTo(expected));
    }
    
    [Test]
    public void MarkAsManager_CreateAllManagers_ReturnsListWithManagers()
    {
        var notManagers = _employees.Where(e => e.IsManager == false).ToList();
        notManagers.ForEach(e => e.MarkAsManager());
        
        var expectedCount = _employees.Count;
        var actualResult = _employees.Count(e => e.IsManager);
        
        Assert.That(expectedCount, Is.EqualTo(actualResult));
    }
}