﻿using Lab3.Tests.Helpers;
using Lab3.Tests.Comparers;
using Lab3.Models;

namespace Lab3.Tests.Tests;

public class ConversionTests
{
    private List<Employee> _employees;
    private List<McDonaldsBranch> _departments;

    [SetUp]
    public void Setup()
    {
        _departments = DataProvider.GenerateDepartments();
        _employees = DataProvider.GenerateEmployees(_departments);
    }    
    
    [Test]
    public void DepartmentsToArray_ConvertsCorrectly()
    {
        var array = _departments.ToArray();

        Assert.That(_departments.Count, Is.EqualTo(array.Length));
        Assert.That(_departments[2].McDonaldsBranchId, Is.EqualTo(array[2].McDonaldsBranchId));
        Assert.That(_departments[2].Name, Is.EqualTo(array[2].Name));
        Assert.That(_departments[2].Employees, Is.EqualTo(array[2].Employees));
    }
    
    [Test]
    public void EmployeesToArray_ConvertsCorrectly()
    {
        var array = _employees.ToArray();

        Assert.That(_employees.Count, Is.EqualTo(array.Length));
        Assert.That(_employees[2].Age, Is.EqualTo(array[2].Age));
        Assert.That(_employees[2].Name, Is.EqualTo(array[2].Name));
        Assert.That(_employees[2].McDonaldsBranchId, Is.EqualTo(array[2].McDonaldsBranchId));
    }
}