﻿using Lab3.Tests.Helpers;
using Lab3.Tests.Comparers;
using Lab3.Models;

namespace Lab3.Tests.Tests;

public class ComparerSortTesting
{
    private List<Employee> _employees;
    private List<McDonaldsBranch> _department;

    [SetUp]
    public void Setup()
    {
        _department = DataProvider.GenerateDepartments();
        _employees = DataProvider.GenerateEmployees(_department);
    }    
    
    [Test]
    public void SortByAge_ReturnsSortedEmployees()
    {
        var expectedSortedNames = new List<string> { "Alice", "Bob", "John" };

        _employees.Sort(new AgeComparer());

        Assert.That(_employees.Select(e => e.Name).ToList(), Is.EqualTo(expectedSortedNames));
    }
    
    [Test]
    public void SortEmployeesByName_ReturnsSortedEmployees()
    {
        var expectedSortedNames = new List<string> { "Alice", "Bob", "John" };
        
        _employees.Sort(new NameComparer());

        Assert.That(_employees.Select(e => e.Name).ToList(), Is.EqualTo(expectedSortedNames));
    }
    
    [Test]
    public void SortDepartmentsByEmployeeCount_ReturnsSortedDepartments()
    {
        var expectedSortedDepartmentsNames = new List<string> { "IT", "HR", "Finance" };
        
        _department.Sort(new EmployeeCountComparer());

        Assert.That(_department.Select(e => e.Name).ToList(), Is.EqualTo(expectedSortedDepartmentsNames));
    }
}