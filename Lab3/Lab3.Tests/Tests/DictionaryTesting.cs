﻿using Lab3.Models;
using Lab3.Tests.Helpers;

namespace Lab3.Tests.Tests;

public class DictionaryTesting
{
    private List<Employee> _employees;
    private List<McDonaldsBranch> _department;

    [SetUp]
    public void Setup()
    {
        _department = DataProvider.GenerateDepartments();
        _employees = DataProvider.GenerateEmployees(_department);
    }
    
    [Test]
    public void Dictionary_CreateEmployeeDictionary_ReturnsIntEmployeeDictionary()
    {
        // Act
        var employeeDictionary = _employees.ToDictionary(employee => employee.EmployeeId);
        var expectedCount = _employees.Count;

        // Assert
        Assert.That(expectedCount, Is.EqualTo(employeeDictionary.Count));
    }
    
    [Test]
    public void Dictionary_CreateDepartmentDictionary_ReturnsEmployeeDepartmentDictionary()
    {
        // Act
        var departmentDictionary = _employees.ToDictionary(employee => employee.McDonaldsBranch);
        var expectedCount = _department.Count;
        
        // Assert
        Assert.That(expectedCount, Is.EqualTo(departmentDictionary.Count));
        Assert.That(departmentDictionary.Keys.FirstOrDefault()?.McDonaldsBranchId == 1, Is.True);
        Assert.That(departmentDictionary.Values.LastOrDefault()?.McDonaldsBranch.Name == "Finance", Is.True);
    }
    
    [Test]
    public void Dictionary_IsManagerDictionary_ReturnsIsManagerDictionary()
    {
        var employeeDictionary = _employees.GroupBy(employee => employee.IsManager)
            .ToDictionary(group => group.Key, group => group.AsEnumerable());

        var expexted = _employees.Count(e => e.IsManager);
        
        Assert.That(expexted, Is.EqualTo(employeeDictionary?.Select(g => g.Key == true).Count()));
    }
    
    [Test]
    public void Dictionary_AgeDictionary_ReturnsEmployeesAgeDictionary()
    {
        var employeeDictionary = _employees.GroupBy(employee => employee.Age)
            .ToDictionary(group => group.Key, group => group.AsEnumerable());

        var expectedCount = _employees.Select(e => e.Age).Distinct().Count();
        
        Assert.That(expectedCount, Is.EqualTo(employeeDictionary?.Count()));
    }
}