﻿using Lab3.Models;
using Lab3.Tests.Helpers;

namespace Lab3.Tests.Tests;

public class SelectTesting
{
    private List<Employee> _employees;
    private List<McDonaldsBranch> _department;

    [SetUp]
    public void Setup()
    {
        _department = DataProvider.GenerateDepartments();
        _employees = DataProvider.GenerateEmployees(_department);
    }

    [Test]
    public void Select_GetNames_ReturnNotEmptyCollection()
    {
        var selectedEmployees = _employees.Select(employee => employee.Name);
        
        Assert.That(selectedEmployees, Is.Not.Empty);
    }
    
    [Test]
    public void Select_GetNames_ReturnCorrectResult()
    {
        var selectedEmployeesNames = _employees.Select(employee => employee.Name);
        var firstEmployeeName = selectedEmployeesNames.FirstOrDefault();
        
        Assert.That(firstEmployeeName, Is.Not.Null);
        Assert.That(firstEmployeeName, Is.EqualTo("John"));
    }
    
    [Test]
    public void Select_GetAges_ReturnCorrectResult()
    {
        var selectedEmployeesAges = _employees.Select(employee => employee.Age);
        var firstEmployeeAge = selectedEmployeesAges.LastOrDefault();

        var expected = _employees.LastOrDefault()?.Age;
        
        Assert.That(firstEmployeeAge, Is.EqualTo(expected));
    }
    
    [Test]
    public void Select_GetDepartmentsFromEmployees_ReturnCorrectResult()
    {
        var selectedDepartments = _employees.Select(employee => employee.McDonaldsBranch).ToList();
        Assert.That(selectedDepartments, Is.Not.Null);
        
        selectedDepartments.RemoveAt(0);
        var expectedCount = _department.Count - 1;
        Assert.That(selectedDepartments.Count(), Is.EqualTo(expectedCount));
    }
    
    [Test]
    public void Select_IsManagersInList_ReturnsTrue()
    {
        var isManagers = _employees.Select(employee => employee.IsManager);
        Assert.That(isManagers.Any(isManager => isManager), Is.True);
    }
}