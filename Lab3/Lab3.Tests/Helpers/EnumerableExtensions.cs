﻿using Lab3.Models;

namespace Lab3.Tests;

public static class EnumerableExtensions
{
    public static double AverageAge(this IEnumerable<Employee> employees)
    {
        if (employees == null || !employees.Any())
            return 0;

        return employees.Average(employee => employee.Age);
    }
    
    public static void MarkAsManager(this Employee employee)
    {
        if (employee == null)
            return;

        employee.IsManager = true;
    }
}