﻿using Lab3.Models;
namespace Lab3.Tests.Helpers;

public class DataProvider
{
    public static List<McDonaldsBranch> GenerateDepartments()
    {
        var departments = new List<McDonaldsBranch>
        {
            new McDonaldsBranch { McDonaldsBranchId = 1, Name = "IT" },
            new McDonaldsBranch { McDonaldsBranchId = 2, Name = "HR" },
            new McDonaldsBranch { McDonaldsBranchId = 3, Name = "Finance" }
        };

        return departments;
    }

    public static List<Employee> GenerateEmployees(List<McDonaldsBranch> departments)
    {
        var employees = new List<Employee>
        {
            new Employee { EmployeeId = 1, Name = "John", Age = 31, IsManager = true, McDonaldsBranchId = 1, McDonaldsBranch = departments[0] },
            new Employee { EmployeeId = 2, Name = "Alice", Age = 25, IsManager = false, McDonaldsBranchId = 2, McDonaldsBranch = departments[1] },
            new Employee { EmployeeId = 3, Name = "Bob", Age = 25, IsManager = true, McDonaldsBranchId = 3, McDonaldsBranch = departments[2] }
       };

        foreach (var employee in employees)
        {
            if (employee.McDonaldsBranch.Employees == null)
            {
                employee.McDonaldsBranch.Employees = new List<Employee> { employee };
            }
            else
            {
                employee.McDonaldsBranch.Employees.Add(employee);
            }
        }

        return employees;
    }
}