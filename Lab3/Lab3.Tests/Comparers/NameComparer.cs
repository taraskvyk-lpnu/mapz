﻿using Lab3.Models;

namespace Lab3.Tests.Comparers;

public class NameComparer : IComparer<Employee>
{
    public int Compare(Employee x, Employee y)
    {
        if (x == null && y == null)
            return 0;
        if (x == null)
            return -1;
        if (y == null)
            return 1;

        return x.Name.CompareTo(y.Name);
    }
}