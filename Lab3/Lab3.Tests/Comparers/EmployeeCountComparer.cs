﻿using Lab3.Models;

namespace Lab3.Tests.Comparers;

public class EmployeeCountComparer : IComparer<McDonaldsBranch>
{
    public int Compare(McDonaldsBranch? x, McDonaldsBranch? y)
    {
        if (x == null && y == null)
            return 0;
        if (x == null)
            return -1;
        if (y == null)
            return 1;

        return x.Employees.Count.CompareTo(y.Employees.Count);
    }
}