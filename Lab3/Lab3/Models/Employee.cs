﻿namespace Lab3.Models;

public class Employee
{
    public int EmployeeId { get; set; }
    public string Name { get; set; }
    public int Age { get; set; }
    public bool IsManager { get; set; }
    public int McDonaldsBranchId { get; set; }
    public McDonaldsBranch McDonaldsBranch { get; set; }
}