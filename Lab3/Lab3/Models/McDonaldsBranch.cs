﻿namespace Lab3.Models;

public class McDonaldsBranch
{
    public int McDonaldsBranchId { get; set; }
    public string Name { get; set; }
    public List<Employee> Employees { get; set; }
}
