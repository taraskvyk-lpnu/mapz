﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

namespace SecondPart
{
    public class MyClass
    {
        private List<int> ints = [];

        public void NonStaticMethod(int x)
        {
            ints.Add(x * x);
        }

        public int NonStaticVariable = 10;
    }

    public static class MyStaticClass
    {
        public static int StaticVariable = 10;
        private static List<int> ints = [];

        public static void StaticMethod(int x)
        {
            ints.Add(x * x);
        }
    }

    internal class Program
    {
        static void Main()
        {
            var obj = new MyClass();    
            var stopwatch = Stopwatch.StartNew();
            
            for (int i = 0; i < 10000000; i++)
            {
                obj.NonStaticMethod(i);
            }
            stopwatch.Stop();
            Console.WriteLine("Час виконання звичайного методу: " + stopwatch.ElapsedMilliseconds + " мс");
            
            stopwatch = Stopwatch.StartNew();
            for (int i = 0; i < 10000000; i++)
            {
                MyStaticClass.StaticMethod(i);
            }
            stopwatch.Stop();
            Console.WriteLine("Час виконання статичного методу: " + stopwatch.ElapsedMilliseconds + " мс");
            
            stopwatch = Stopwatch.StartNew();
            for (int i = 0; i < 10000000; i++)
            {
                obj.NonStaticVariable++;
            }
            stopwatch.Stop();
            Console.WriteLine("Час доступу до звичайної змінної: " + stopwatch.ElapsedMilliseconds + " мс");
            
            stopwatch = Stopwatch.StartNew();
            for (int i = 0; i < 10000000; i++)
            {
                MyStaticClass.StaticVariable++;
            }
            stopwatch.Stop();
            Console.WriteLine("Час доступу до статичної змінної: " + stopwatch.ElapsedMilliseconds + " мс");
        }
    }
}