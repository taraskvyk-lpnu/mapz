﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstPart
{
    internal class Program
    {
        public interface IChargable
        {
            void Charge();
        }
        
        public struct Tesla : IChargable
        {
            public string Model { get; private set; }

            //public Tesla(string model)
            //{
            //    Model = model;
            //}

            public double CalculateChargingCost()
            {
                double money = new Random().NextDouble() * 100;
                return Math.Round(money, 3);
            }

            public void Charge()
            {
                Console.WriteLine($"I'm charging \'{Model}\'");
            }
        }

        static void Main(string[] args)
        {
            Tesla tesla = new Tesla();
            Console.WriteLine($"\'{tesla.Model}\' charging costs {tesla.CalculateChargingCost()}$");
            tesla.Charge();
        }
    }
}
