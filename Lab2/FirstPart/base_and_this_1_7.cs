﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace FirstPart
{
    //internal class Program
    //{
    //    public abstract class Car
    //    {
    //        public string Model { get; private set; }

    //        protected Car() : this("Unknown") { }
    //        protected Car(string model) => Model = model;

    //        public abstract double CalculateChargingCost();

    //        protected void Introduce() => Console.WriteLine("I'm a car");
    //    }

    //    public interface IChargable
    //    {
    //        void Charge();
    //    }

    //    public class Tesla : Car, IChargable
    //    {
    //        public Tesla() : base() { }
    //        public Tesla(string model) : base(model) { }

    //        public override double CalculateChargingCost() => Math.Round(new Random().NextDouble() * 100, 3);
    //        public void Charge() => Console.WriteLine($"I'm charging \'{Model}\'");

    //        public void Introduce()
    //        {
    //            base.Introduce();
    //            Console.WriteLine($"My model is \'{Model}\'");
    //        }
    //    }

    //    static void Main(string[] args)
    //    {
    //        Car tesla = new Tesla();
    //        Console.WriteLine(tesla.Model);
    //        ((Tesla)tesla).Introduce();
    //    }
    //}
}
