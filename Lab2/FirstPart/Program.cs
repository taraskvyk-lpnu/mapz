﻿using System;
using System.Reflection;

namespace FirstPart
{
    //internal class Program
    //{
    //    public abstract class Car
    //    {
    //        public string Model { get; private set; }

    //        protected Car(string model)
    //        {
    //            Model = model;
    //        }

    //        public abstract double CalculateChargingCost();
    //    }

    //    public interface IChargable
    //    {
    //        void Charge();
    //    }

    //    public class Tesla : Car, IChargable
    //    {
    //        public Tesla(string model) : base(model) { }

    //        public override double CalculateChargingCost()
    //        {
    //            double money = new Random().NextDouble() * 100;
    //            return Math.Round(money, 3);
    //        }

    //        public void Charge()
    //        {
    //            Console.WriteLine($"I'm charging \'{Model}\'");
    //        }
    //    }

    //    static void Main(string[] args)
    //    {
    //        Car tesla = new Tesla("Model X");
    //        Console.WriteLine($"\'{tesla.Model}\' charging costs {tesla.CalculateChargingCost()}$");

    //        IChargable teslaCharge = tesla as IChargable;
    //        teslaCharge.Charge();
    //    }
    //}
}
