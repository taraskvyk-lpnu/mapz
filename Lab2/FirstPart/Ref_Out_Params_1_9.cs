﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstPart
{
    //class Program
    //{
    //    public static void Add(int a, int b) => Console.WriteLine($"Result: {a + b}");
    //    public static void Divide(int a, int b, out int result) => result = (b != 0) ? (a / b) : 0;
    //    public static void Increment(ref int num) => num++;
    //    public static void Display(in int value) => Console.WriteLine($"Value: {value}");

    //    public static void Main(string[] args)
    //    {
    //        int x = 10, y = 20;
    //        Add(x, y);
            
    //        int num1 = 10, num2 = 0, result;
    //        Divide(num1, num2, out result);
    //        Console.WriteLine($"Result: {result}");

    //        int number = 5;
    //        Increment(ref number);
    //        Console.WriteLine($"Number: {number}"); 

    //        Display(in number);
    //    }
    //}
}
