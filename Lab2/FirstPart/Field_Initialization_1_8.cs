﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstPart
{
    //class Program
    //{
    //    public class MyClass
    //    {
    //        public static string StaticField1 = "Static field 1";
    //        public static string StaticField2;
    //        public string InstanceField1 = "Instance field 1";
    //        public string InstanceField2;

    //        static MyClass()
    //        {
    //            StaticField2 = "Static field 2 (initialized in static constructor)";
    //            Console.WriteLine("Static constructor called");
    //        }

    //        public MyClass()
    //        {
    //            InstanceField2 = "Instance field 2 (initialized in instance constructor)";
    //            Console.WriteLine("Instance constructor called");
    //        }
    //    }

    //    static void Main(string[] args)
    //    {
    //        Console.WriteLine("Creating first instance:");
    //        MyClass obj = new MyClass();

    //        Console.WriteLine($"\nobj.StaticField1: {MyClass.StaticField1}");
    //        Console.WriteLine($"obj.StaticField2: {MyClass.StaticField2}");
    //        Console.WriteLine($"obj.InstanceField1: {obj.InstanceField1}");
    //        Console.WriteLine($"obj.InstanceField2: {obj.InstanceField2}");
    //    }
    //}
}
